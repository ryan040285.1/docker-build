```
docker-compose up -d 启动
docker-compose up -d --build 强制编译
docker-compose up -d --force-recreate 强制重建容器
```



https://note.qidong.name/2017/06/26/docker-clean/

清理所有停止运行的容器：

docker container prune
# or
docker rm $(docker ps -aq)
清理所有悬挂（<none>）镜像：

docker image prune
# or
docker rmi $(docker images -qf "dangling=true")
清理所有无用数据卷：

docker volume prune


--------------------------------------------------------------------------
https://colobu.com/2018/05/15/Stop-and-remove-all-docker-containers-and-images/

删除所有的镜像
docker rmi $(docker images -q)


docker exec -it id bash